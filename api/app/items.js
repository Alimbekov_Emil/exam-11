const path = require("path");
const express = require("express");
const { nanoid } = require("nanoid");
const Item = require("../models/Item");
const auth = require("../middleware/auth");
const multer = require("multer");
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    if (req.query.category) {
      const categoriesItems = await Item.find({ category: req.query.category }).populate("category");
      res.send(categoriesItems);
    } else {
      const items = await Item.find();
      return res.send(items);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const item = await Item.findOne({ _id: req.params.id }).populate("category").populate("seller");
    res.send(item);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
  try {
    const itemData = { ...req.body, seller: req.user._id };

    if (req.file) {
      itemData.image = "uploads/" + req.file.filename;
    }
    const item = new Item(itemData);

    await item.save();

    res.send(item);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.delete("/:id", auth, async (req, res) => {
  try {
    const item = await Item.findOne({ _id: req.params.id });

    const seller = item.seller;
    const user = req.user._id;

    if (seller.toString() !== user.toString()) {
      return res.status(403).send(e);
    }

    await Item.findOneAndDelete({ _id: req.params.id });

    return res.send("Your Item Deleted");
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
