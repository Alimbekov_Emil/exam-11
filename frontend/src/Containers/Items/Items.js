import { Grid, Typography } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchItems } from "../../store/actions/itemsActions";
import Category from "../Category/Category";
import ItemsItem from "./ItemsItem";

const Items = () => {
  const dispatch = useDispatch();

  const items = useSelector((state) => state.items.items);

  useEffect(() => {
    dispatch(fetchItems());
  }, [dispatch]);

  return (
    <Grid container justify="space-around">
      <Grid item style={{ width: "30%" }}>
        <Category />
      </Grid>
      <Grid item style={{ width: "70%" }}>
        <Typography variant="h3">All Items</Typography>
        <Grid item container style={{ display: "flex", flexWrap: "wrap" }}>
          {items
            ? items.map((item) => (
                <ItemsItem
                  key={item._id}
                  id={item._id}
                  title={item.title}
                  price={item.price}
                  image={item.image}
                />
              ))
            : null}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Items;
