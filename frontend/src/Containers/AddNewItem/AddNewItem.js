import { Button, Grid, MenuItem, TextField, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import FileInput from "../../Components/UI/Form/FileInput";
import FormElement from "../../Components/UI/Form/FormElement";
import { fetchCategories } from "../../store/actions/categoryActions";
import { historyPush } from "../../store/actions/historyActions";
import { postItems } from "../../store/actions/itemsActions";

const AddNewItem = () => {
  const dispatch = useDispatch();
  const [item, setItem] = useState({
    title: "",
    description: "",
    image: "",
    category: "",
    price: "",
  });

  const user = useSelector((state) => state.users.user);
  const categories = useSelector((state) => state.category.categories);

  useEffect(() => {
    dispatch(fetchCategories());
  }, [dispatch]);

  if (user === null) {
    dispatch(historyPush("/login"));
  }

  const inputChangeHandler = (e) => {
    const { name, value } = e.target;

    setItem((prev) => ({ ...prev, [name]: value }));
  };

  const submitFormHandler = (e) => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(item).forEach((key) => {
      formData.append(key, item[key]);
    });

    dispatch(postItems(formData));
  };

  const fileChangeHandler = (e) => {
    const name = e.target.name;
    const file = e.target.files[0];

    setItem((prevState) => ({
      ...prevState,
      [name]: file,
    }));
  };

  return (
    <>
      <Typography variant="h3" style={{ margin: "30px 0" }}>
        Add New Item
      </Typography>
      <Grid container spacing={4} direction="column" component="form" onSubmit={submitFormHandler}>
        <Grid item xs>
          <FormElement
            label="Title"
            type="text"
            onChange={inputChangeHandler}
            name="title"
            required={true}
            value={item.title}
          />
        </Grid>

        <Grid item xs>
          <FormElement
            label="Description"
            type="text"
            onChange={inputChangeHandler}
            name="description"
            required={true}
            value={item.description}
          />
        </Grid>

        <Grid item xs>
          <FormElement
            label="Price"
            type="text"
            onChange={inputChangeHandler}
            name="price"
            value={item.price}
            required={true}
          />
        </Grid>

        <FileInput name="image" label="Image" onChange={fileChangeHandler} />

        <Grid item xs>
          <TextField
            required
            fullWidth
            select
            variant="outlined"
            label="Category"
            name="category"
            value={item.category}
            onChange={inputChangeHandler}
          >
            <MenuItem disabled>
              <i>Select a category</i>
            </MenuItem>
            {categories.map((category) => (
              <MenuItem key={category._id} value={category._id}>
                {category.title}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        <Button variant="contained" color="secondary" type="submit">
          Create Item
        </Button>
      </Grid>
    </>
  );
};

export default AddNewItem;
