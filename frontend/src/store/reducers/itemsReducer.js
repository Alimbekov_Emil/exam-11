import {
  DELETE_ITEM_FAILURE,
  DELETE_ITEM_REQUEST,
  DELETE_ITEM_SUCCESS,
  FETCH_ITEMSCATEGORY_FAILURE,
  FETCH_ITEMSCATEGORY_REQUEST,
  FETCH_ITEMSCATEGORY_SUCCESS,
  FETCH_ITEMS_FAILURE,
  FETCH_ITEMS_REQUEST,
  FETCH_ITEMS_SUCCESS,
  FETCH_ITEM_FAILURE,
  FETCH_ITEM_REQUEST,
  FETCH_ITEM_SUCCESS,
} from "../actions/itemsActions";

const initialState = {
  items: [],
  item: null,
  itemsCategory: [],
  itemsLoading: false,
  itemsError: false,
};

const itemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ITEMS_REQUEST:
      return { ...state, itemsLoading: true };
    case FETCH_ITEMS_SUCCESS:
      return { ...state, itemsLoading: false, items: action.items };
    case FETCH_ITEMS_FAILURE:
      return { ...state, itemsLoading: false, itemsError: action.error };
    case FETCH_ITEM_REQUEST:
      return { ...state, itemsLoading: true };
    case FETCH_ITEM_SUCCESS:
      return { ...state, itemsLoading: false, item: action.item };
    case FETCH_ITEM_FAILURE:
      return { ...state, itemsLoading: false, error: action.error };
    case FETCH_ITEMSCATEGORY_REQUEST:
      return { ...state, itemsLoading: true };
    case FETCH_ITEMSCATEGORY_SUCCESS:
      return { ...state, itemsLoading: false, itemsCategory: action.itemsCategory };
    case FETCH_ITEMSCATEGORY_FAILURE:
      return { ...state, itemsLoading: false, error: action.error };
    case DELETE_ITEM_REQUEST:
      return { ...state, itemsLoading: true };
    case DELETE_ITEM_SUCCESS:
      return { ...state, itemsLoading: false };
    case DELETE_ITEM_FAILURE:
      return { ...state, itemsLoading: false, error: action.error };
    default:
      return state;
  }
};
export default itemsReducer;
