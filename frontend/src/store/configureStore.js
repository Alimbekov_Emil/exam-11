import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import categoryReducer from "./reducers/categoryReducers";
import itemsReducer from "./reducers/itemsReducer";
import usersReducer from "./reducers/userReducers";

const saveToLocalStorage = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("forumState", serializedState);
  } catch (e) {
    console.log("Could not save state");
  }
};

const loadFromLocalStorage = () => {
  try {
    const serializedState = localStorage.getItem("forumState");
    if (serializedState === null) {
      return undefined;
    }

    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};

const rootReducer = combineReducers({
  users: usersReducer,
  category: categoryReducer,
  items: itemsReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunkMiddleware)));

store.subscribe(() => {
  saveToLocalStorage({
    users: store.getState().users,
  });
});

export default store;
