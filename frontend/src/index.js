import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Router } from "react-router-dom";
import { NotificationContainer } from "react-notifications";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core";
import { Provider } from "react-redux";
import store from "./store/configureStore";
import history from "./history";
import "react-notifications/lib/notifications.css";

const theme = createMuiTheme({
  props: {
    MuiTextField: {
      variant: "outlined",
      fullWidth: true,
    },
  },
});

const app = (
  <Provider store={store}>
    <Router history={history}>
      <MuiThemeProvider theme={theme}>
        <NotificationContainer />
        <App />
      </MuiThemeProvider>
    </Router>
  </Provider>
);

ReactDOM.render(app, document.getElementById("root"));
