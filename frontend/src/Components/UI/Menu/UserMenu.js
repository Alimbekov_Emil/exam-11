import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { logoutUser } from "../../../store/actions/usersActions";

const UserMenu = ({ user }) => {
  const dispatch = useDispatch();

  return (
    <>
      <b>Hello, {user.displayName} ! </b>
      <Link style={{ color: "black", textDecoration: "none" }} to="/new-item">
        Add New item{" "}
      </Link>
      or
      <button
        style={{ background: "inherit", outline: "none", border: "none", cursor: "pointer" }}
        onClick={() => dispatch(logoutUser())}
      >
        Logout
      </button>
    </>
  );
};

export default UserMenu;
