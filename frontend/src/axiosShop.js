import axios from "axios";
import { apiURL } from "./config";

const axiosShop = axios.create({
  baseURL: apiURL,
});

export default axiosShop;
